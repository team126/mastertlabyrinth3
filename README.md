# README #

This README would normally document whatever steps are necessary to get your application up and running. It does not.

### What is this repository for? ###

Team 126 in Alphonce Spring2016 CSE116

### Contribution guidelines ###

* only 3 people in the group are allowed to commit changes. these 3 people are all in group chat.
* Whenever a person wishes to code a file, they must message the groupchat that they claim that file.
* only unclaimed files can be claimed. after a person has claimed, edited, and pushed the changes back to git 
* that person must message groupchat declaring that they are no longer editing the file, and that file becomes unclaimed.